import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store/index";
import vuetify from "./plugins/vuetify";
import VueCompositionApi from "@vue/composition-api";

Vue.use(VueCompositionApi);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  vuetify,
  render: h => h(App)
}).$mount("#app");
