import Vue from "vue";
import Vuex from "vuex";
import ui from "./modules/ui";
import cart from "./modules/cart";
import merchandise from "./modules/merchandise";
import user from "./modules/user";
import order from "./modules/order";
import admin from "./modules/admin";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { ui, cart, merchandise, user, order, admin }
});
