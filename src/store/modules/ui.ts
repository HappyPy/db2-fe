interface State {
  drawer: boolean;
  barTitle: string;
}

export default {
  state: {
    drawer: false,
    barTitle: "ElectroShop"
  },
  getters: {},
  mutations: {
    setDrawer(state: State, drawer: boolean) {
      state.drawer = drawer;
    },
    setTitle(state: State, title: string) {
      state.barTitle = title;
    }
  }
};
