import { assoc, concat, filter, map, replace } from "ramda";

const toBase64 = file =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

const defaultMerchData = {
  name: "",
  price: null,
  description: "",
  shortDescription: ""
};
export default {
  state: {
    currentMerch: defaultMerchData,
    merch: [],
    loading: false,
    orderLoading: false,
    totalCount: null,
    newPhotos: [],
    orders: []
  },
  mutations: {
    setCurrentMerch(state, merch) {
      state.currentMerch = merch;
    },
    setCurrentMerchCategories(state, categories) {
      const mappedCategories = map(id => {
        return { category: { id }, merchandise: { id: state.currentMerch.id } };
      }, categories);
      state.currentMerch.categories = mappedCategories;
    },
    clearCurrentMerch(state) {
      state.currentMerch = defaultMerchData;
      state.newPhotos = [];
    },
    removePhoto(state, photo) {
      state.currentMerch.photos = filter(
        ph => ph.id !== photo.id,
        state.currentMerch.photos
      );
    },
    setNewPhoto(state, photo) {
      const photoStrings = map(async file => toBase64(file), photo);
      Promise.all(photoStrings).then(all => {
        state.newPhotos = all;
      });
    },
    replaceAdminMerch(state, merch) {
      state.merch = merch;
    },
    setTotalCount(state, count) {
      state.totalCount = count;
    },
    setAdminLoading(state, loading) {
      state.loading = loading;
    },
    setOrderLoading(state, loading) {
      state.orderLoading = loading;
    },
    setOrders(state, orders) {
      state.orders = orders;
    }
  },
  actions: {
    loadAdminMerch({ state, commit }, pageable) {
      const baseUrl = new URL("http://localhost:8080/merchandise/all/page");
      baseUrl.searchParams.append("page", pageable.page);
      baseUrl.searchParams.append("size", pageable.size);
      commit("setAdminLoading", true);
      return fetch(baseUrl.toString(), {
        method: "GET",
        mode: "cors",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(response => {
          return response.json();
        })
        .then(({ data }) => {
          commit("replaceAdminMerch", data);
        })
        .catch(e => {
          console.error("fetch failed", e);
        })
        .finally(() => {
          commit("setAdminLoading", false);
        });
    },
    loadTotalCount({ state, commit }) {
      const baseUrl = new URL("http://localhost:8080/merchandise/count");
      commit("setAdminLoading", true);
      return fetch(baseUrl.toString(), {
        method: "GET",
        mode: "cors",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(response => {
          return response.json();
        })
        .then(({ data }) => {
          commit("setTotalCount", data);
        })
        .catch(e => {
          console.error("fetch failed", e);
        })
        .finally(() => {
          commit("setAdminLoading", false);
        });
    },
    saveCurrentMerch: function({ state, commit, rootState }) {
      const baseUrl = new URL("http://localhost:8080/admin/merchandise");
      commit("setAdminLoading", true);
      const newPhotoString = map(im => {
        return { photo: replace(/(data:.*;base64),/, "", im) };
      }, state.newPhotos);
      const merged = concat(newPhotoString, state.currentMerch.photos);
      return fetch(baseUrl.toString(), {
        method: "PUT",
        mode: "cors",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          Authorization: rootState.user.token
        },
        body: JSON.stringify(assoc("photos", merged, state.currentMerch))
      })
        .then(response => {
          return response.json();
        })
        .catch(e => {
          console.error("fetch failed", e);
        })
        .finally(() => {
          commit("setAdminLoading", false);
        });
    },
    deleteMerch({ state, commit, rootState }, id) {
      const baseUrl = new URL(`http://localhost:8080/admin/merchandise/${id}`);
      commit("setAdminLoading", true);
      return fetch(baseUrl.toString(), {
        method: "DELETE",
        mode: "cors",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          Authorization: rootState.user.token
        }
      })
        .then(response => {
          return response.json();
        })
        .catch(e => {
          console.error("fetch failed", e);
        })
        .finally(() => {
          commit("setAdminLoading", false);
        });
    },
    loadOrders({ state, commit, rootState }) {
      const baseUrl = new URL(`http://localhost:8080/admin/orders`);
      commit("setOrderLoading", true);
      return fetch(baseUrl.toString(), {
        method: "GET",
        mode: "cors",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          Authorization: rootState.user.token
        }
      })
        .then(response => {
          return response.json();
        })
        .then(({ data }) => {
          commit("setOrders", data);
        })
        .catch(e => {
          console.error("fetch failed", e);
        })
        .finally(() => {
          commit("setOrderLoading", false);
        });
    }
  }
};
