import { all, filter, identity, isEmpty, mapObjIndexed, values } from "ramda";
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import { isNilOrEmpty, isNotEmpty, isObject } from "ramda-extension";

const STORAGE_TOKEN = "auth";

const persist = (data: string) => {
  localStorage.setItem(STORAGE_TOKEN, data);
};

interface State {
  username: string;
  token: string;
  userError: string;
  userInfo: Register;
}

interface Login {
  username: string;
  password: string;
}

interface Register {
  username: string;
  password: string;
  user: {
    name: string;
    surname: string;
    address: {
      city: string;
      street: string;
      houseNumber: string;
      postCode: string;
    };
  };
}

export default {
  state: {
    username: "",
    token: "",
    userError: "",
    userInfo: null
  },
  getters: {
    isLogged: (state: State) => () => {
      const storageToken = localStorage.getItem(STORAGE_TOKEN);
      if (storageToken) {
        state.token = storageToken;
      }
      return !isEmpty(state.token);
    },
    isAdmin: (state: State) => {
      if (state.userInfo) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        return state.userInfo.loginType === "ADMINISTRATOR";
      }
      return false;
    }
  },
  mutations: {
    setToken(state: State, token: string) {
      state.token = token;
      persist(token);
    },
    setErrors(state: State, errors: string) {
      state.userError = errors;
    },
    setUserInfo(state: State, user: Register) {
      state.userInfo = user;
    }
  },
  actions: {
    isAdmin: ({ commit }: { commit: Function }, state: State) => () => {
      return state.userInfo;
    },
    logout({ commit }: { commit: Function }) {
      commit("setToken", "");
      localStorage.removeItem(STORAGE_TOKEN);
    },
    login(
      { commit, dispatch }: { commit: Function; dispatch: Function },
      login: Login
    ) {
      const baseUrl = new URL("http://localhost:8080/login");
      return fetch(baseUrl.toString(), {
        method: "post",
        mode: "cors",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(login)
      }).then(response => {
        if (response.ok) {
          commit("setToken", response.headers.get("Authorization"));
          dispatch("loadUserInfo");
          return;
        } else {
          commit("setErrors", "Neúspěšné příhlášní");
          throw new Error("Something went wrong");
        }
      });
    },
    register(
      { commit, dispatch }: { commit: Function; dispatch: Function },
      register: Register
    ) {
      const filtered = filter(
        obj =>
          !isObject(obj) ||
          all(identity, values(mapObjIndexed(val => isNotEmpty(val), obj))),
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        register
      );
      const baseUrl = new URL("http://localhost:8080/users/signUp");
      return fetch(baseUrl.toString(), {
        method: "post",
        mode: "cors",
        headers: {
          "Content-Type": "application/json"
        },
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        body: JSON.stringify(filtered)
      })
        .then(response => {
          if (response.ok) {
            return response.json();
          } else {
            commit("setErrors", "Neúspěšná registrace");
            throw new Error("Something went wrong");
          }
        })
        .then(data => {
          if (isNilOrEmpty(data.errors)) {
            return dispatch("login", {
              username: register.username,
              password: register.password
            });
          } else {
            commit("setErrors", data.errors[0].message);
            throw new Error("Something went wrong");
          }
        });
    },
    loadUserInfo({ commit, state }: { commit: Function; state: State }) {
      const baseUrl = new URL("http://localhost:8080/users/");
      return fetch(baseUrl.toString(), {
        method: "get",
        mode: "cors",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          Authorization: state.token
        }
      })
        .then(response => response.json())
        .then(({ data }) => {
          return commit("setUserInfo", data);
        });
    }
  }
};
