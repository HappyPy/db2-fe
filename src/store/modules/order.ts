import { assoc, dissoc, map } from "ramda";
import { CartItem } from "@/store/modules/cart";
import merchandise from "@/store/modules/merchandise";

const STORAGE_TOKEN = "auth";

const persist = (data: string) => {
  localStorage.setItem(STORAGE_TOKEN, data);
};

interface State {
  orders: Order[];
}

interface Order {
  payment: {
    paymentType: string;
    payed: string;
  };
  created: string;
  user: {
    name: string;
    surname: string;
    address: {
      city: string;
      street: string;
      houseNumber: string;
      postCode: string;
    };
  };
  items: CartItem[];
}

export default {
  state: {
    orders: []
  },
  getters: {},
  mutations: {
    setOrders(state: State, orders: Order[]) {
      state.orders = orders;
    }
  },
  actions: {
    createOrder(
      {
        commit,
        rootState
      }: { commit: Function; rootState: Record<string, any> },
      order: Order
    ) {
      const baseUrl = new URL("http://localhost:8080/orders/create");
      let price = 0;
      const merchMapped = map(item => {
        price += item.price * item.count;
        return {
          count: item.count,
          price: item.price,
          merchId: item.id
        };
      }, order.items);
      const withPrice = assoc("price", price, order);
      const withoutItems = assoc(
        "merchandises",
        merchMapped,
        dissoc("items", withPrice)
      );
      return fetch(baseUrl.toString(), {
        method: "post",
        mode: "cors",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          Authorization: rootState.user.token
        },
        body: JSON.stringify(withoutItems)
      }).then(response => {
        if (response.ok) {
          commit("clearCart");
          return response.json();
        } else {
          throw new Error("Something went wrong");
        }
      });
    },
    getOrders({
      commit,
      rootState
    }: {
      commit: Function;
      rootState: Record<string, any>;
    }) {
      const baseUrl = new URL("http://localhost:8080/orders/");
      return fetch(baseUrl.toString(), {
        method: "get",
        mode: "cors",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          Authorization: rootState.user.token
        }
      })
        .then(response => response.json())
        .then(({ data }) => {
          return commit("setUserInfo", data);
        });
    }
  }
};
