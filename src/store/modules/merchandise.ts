import {
  compose,
  equals,
  filter,
  find,
  forEachObjIndexed,
  head,
  isEmpty,
  map,
  mergeDeepLeft,
  mergeDeepRight,
  prop,
  propEq
} from "ramda";

interface MerchandiseCategory {
  id: number;
  photos: string;
}

interface MerchandiseItem {
  id: number;
  name: string;
  price: string;
  availableInWarehouse: string;
  description: string;
  photos: [string];
  categories: [MerchandiseCategory];
}
interface MerchandiseFilter {
  priceMin: number;
  priceMax: number;
  categories: string[];
  page: number;
  size: number;
}
interface MerchandiseState {
  items: [MerchandiseItem];
  filter: MerchandiseFilter;
  loading: boolean;
}

const defaultFilter: MerchandiseFilter = {
  priceMin: 0,
  priceMax: 0,
  categories: [],
  page: 0,
  size: 25
};

export default {
  state: {
    filter: defaultFilter,
    items: [],
    loading: false
  },
  getters: {
    getMerchById: (state: MerchandiseState) => (idToGet: number) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      return head(filter(item => prop("id", item) == idToGet)(state.items));
    }
  },
  mutations: {
    replaceMerchandiseItems(state: MerchandiseState, items: [MerchandiseItem]) {
      state.items = items;
    },
    setLoading(state: MerchandiseState, loading: boolean) {
      state.loading = loading;
    },
    setFilter(
      state: MerchandiseState,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      filter: Record<string, any>
    ) {
      state.filter = mergeDeepRight(state.filter, filter);
    },
    clearFilter(state: MerchandiseState) {
      state.filter = defaultFilter;
    }
  },
  actions: {
    fetchMerchandise({
      state,
      commit
    }: {
      state: MerchandiseState;
      commit: Function;
    }) {
      const baseUrl = new URL("http://localhost:8080/merchandise/");
      compose(
        forEachObjIndexed((value, key) => {
          if (value && !isEmpty(value)) {
            baseUrl.searchParams.append(key, value);
          }
        }),
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        prop("filter")
      )(state);
      commit("setLoading", true);
      return fetch(baseUrl.toString(), {
        method: "GET",
        mode: "cors",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(response => {
          return response.json();
        })
        .then(({ data }: { data: [MerchandiseItem] }) => {
          commit("replaceMerchandiseItems", data);
        })
        .catch(e => {
          console.error("fetch failed", e);
        })
        .finally(() => {
          commit("setLoading", false);
        });
    },
    loadMerch(
      {
        state,
        commit
      }: {
        state: MerchandiseState;
        commit: Function;
      },
      id: number
    ) {
      commit("setLoading", true);
      const baseUrl = new URL(`http://localhost:8080/merchandise/${id}`);
      return fetch(baseUrl.toString(), {
        method: "GET",
        mode: "cors",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(response => {
          return response.json();
        })
        .then(({ data }: { data: [MerchandiseItem] }) => {
          commit("replaceMerchandiseItems", data);
        })
        .catch(e => {
          console.error("fetch failed", e);
        })
        .finally(() => {
          commit("setLoading", false);
        });
    },
    changeFilter(
      {
        state,
        commit
      }: {
        state: MerchandiseState;
        commit: Function;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      },
      filter: Record<string, any>
    ) {
      commit("setFilter", filter);
    }
  }
};
