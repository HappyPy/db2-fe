import {
  any,
  append,
  inc,
  curry,
  eqProps,
  equals,
  filter,
  head,
  lensProp,
  ifElse,
  length,
  map,
  not,
  over,
  prop,
  propEq,
  when,
  assoc,
  dec,
  identity
} from "ramda";

const STORAGE_CART_ITEMS = "cartItems";

const persist = (data: CartItem[]) => {
  localStorage.setItem(STORAGE_CART_ITEMS, JSON.stringify(data));
};

interface State {
  items: CartItem[];
  loaded: boolean;
}
export interface CartItem {
  id: string;
  name: string;
  count: number;
  price: number;
}
export default {
  state: {
    items: [],
    loaded: false
  },
  getters: {
    itemCount: (state: State) => () => {
      return length(state.items);
    },
    getItem: (state: State) => (idToGet: string) => {
      return head(filter(({ id }) => equals(id, idToGet), state.items));
    }
  },
  mutations: {
    addItem(state: State, item: CartItem) {
      const countUp = curry((key, items) =>
        map(when(propEq("id", key), over(lensProp("count"), inc)), items)
      );
      state.items = ifElse(
        any(eqProps("id", item)),
        countUp(prop("id", item)),
        append(assoc("count", 1, item))
      )(state.items);
      persist(state.items);
    },
    removeOneItem(state: State, item: CartItem) {
      const countDown = curry((key, items) =>
        map(when(propEq("id", key), over(lensProp("count"), dec)), items)
      );
      if (item.count > 1) {
        state.items = ifElse(
          any(eqProps("id", item)),
          countDown(prop("id", item)),
          identity
        )(state.items);
      }
      persist(state.items);
    },
    removeItem(state: State, idToRemove: string) {
      state.items = filter(
        ({ id }) => not(equals(id, idToRemove)),
        state.items
      );
      persist(state.items);
    },
    setCartItems(state: State, items: [CartItem]) {
      state.items = items;
    },
    clearCart(state: State) {
      state.items = [];
      persist([]);
    }
  },
  actions: {
    loadCartItems({ commit }: { commit: Function }) {
      const itemString = localStorage.getItem(STORAGE_CART_ITEMS);
      commit("setCartItems", JSON.parse(itemString ? itemString : "[]"));
    }
  }
};
