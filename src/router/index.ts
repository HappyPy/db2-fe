import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Categories from "../views/Categories.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/kategorie",
    name: "Kategorie",
    component: Categories
  },
  {
    path: "/about",
    name: "About",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue")
  },
  {
    path: "/kosik",
    name: "Kosik",
    component: () => import(/* webpackChunkName: "cart" */ "../views/Cart.vue")
  },
  {
    path: "/objednavka",
    name: "order",
    component: () =>
      import(/* webpackChunkName: "order" */ "../views/Order.vue")
  },
  {
    path: "/vysledek",
    name: "Result",
    component: () =>
      import(/* webpackChunkName: "result" */ "../views/Result.vue")
  },
  {
    path: "/zbozi/:id",
    name: "Merchandise",
    component: () =>
      import(/* webpackChunkName: "Merchandise" */ "../views/Merchandise.vue")
  },
  {
    path: "/ucet",
    name: "User",
    component: () => import(/* webpackChunkName: "User" */ "../views/User.vue")
  },
  {
    path: "/admin",
    name: "Admin",
    component: () =>
      import(/* webpackChunkName: "Admin" */ "../views/Admin.vue")
  },
  {
    path: "/camera",
    name: "Camera",
    component: () =>
      import(
        /* webpackChunkName: "FilteredMerchandise" */ "../views/FilteredMerchandise.vue"
      ),
    props: { categories: ["CAMERA"] }
  },
  {
    path: "/tablety",
    name: "tablety",
    component: () =>
      import(
        /* webpackChunkName: "FilteredMerchandise" */ "../views/FilteredMerchandise.vue"
      ),
    props: { categories: ["TABLET"] }
  },
  {
    path: "/notebooky",
    name: "Notebooky",
    component: () =>
      import(
        /* webpackChunkName: "FilteredMerchandise" */ "../views/FilteredMerchandise.vue"
      ),
    props: { categories: ["NOTEBOOK"] }
  },
  {
    path: "/pocitace",
    name: "Pocitace",
    component: () =>
      import(
        /* webpackChunkName: "FilteredMerchandise" */ "../views/FilteredMerchandise.vue"
      ),
    props: { categories: ["COMPUTER"] }
  },
  {
    path: "/telefony",
    name: "Telefony",
    component: () =>
      import(
        /* webpackChunkName: "FilteredMerchandise" */ "../views/FilteredMerchandise.vue"
      ),
    props: { categories: ["PHONE"] }
  },
  {
    path: "/herni_konzole",
    name: "Herni konzole",
    component: () =>
      import(
        /* webpackChunkName: "FilteredMerchandise" */ "../views/FilteredMerchandise.vue"
      ),
    props: { categories: ["GAME_CONSOLE"] }
  },
  {
    path: "/Monitory",
    name: "Monitory",
    component: () =>
      import(
        /* webpackChunkName: "FilteredMerchandise" */ "../views/FilteredMerchandise.vue"
      ),
    props: { categories: ["MONITOR"] }
  },
  {
    path: "/TV",
    name: "TV",
    component: () =>
      import(
        /* webpackChunkName: "FilteredMerchandise" */ "../views/FilteredMerchandise.vue"
      ),
    props: { categories: ["TV"] }
  },
  {
    path: "/video",
    name: "Video",
    component: () =>
      import(
        /* webpackChunkName: "FilteredMerchandise" */ "../views/FilteredMerchandise.vue"
      ),
    props: { categories: ["VIDEO"] }
  },
  {
    path: "/audio",
    name: "Audio",
    component: () =>
      import(
        /* webpackChunkName: "FilteredMerchandise" */ "../views/FilteredMerchandise.vue"
      ),
    props: { categories: ["AUDIO"] }
  }
];

const router = new VueRouter({
  routes
});

export default router;
